import type { DenonConfig } from "https://deno.land/x/denon/mod.ts";

const config: DenonConfig = {
  scripts: {
    start: {
      cmd: "deno run src/index.ts",
      desc: "run my index.ts file",
      allow: ["net", "read"],
    },
  },
};

export default config;
