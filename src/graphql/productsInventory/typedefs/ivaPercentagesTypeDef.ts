export const ivaPercentagesTypeDef = `
type Query {
    ivaPercentages: [IvaPercentage]
}

type IvaPercentage {
    id: Int
    value: Int
    created_at: DateTime
    modified_at: DateTime
}
`;
