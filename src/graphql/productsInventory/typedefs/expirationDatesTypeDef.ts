export const expirationDatesTypeDef = `
scalar Date
scalar DateTime

type Query {
    expirationDates: [ExpirationDate]
}

type Mutation {
    createExpirationDate(expirationDate: CreateExpirationDate): ExpirationDate
    updateExpirationDate(id: Long!, expirationDate: UpdateExpirationDate!): ExpirationDate
    deleteExpirationDate(id: Int!): ExpirationDate 
}

type ExpirationDate {
    id: Long
    value: Date
    product: Product
    created_at: DateTime
    modified_at: DateTime
}
input CreateExpirationDate {
    value: Date!
    product_id: Long!
}
input UpdateExpirationDate {
    value: Date
    product_id: Long
}
`;
