export const productsTypeDef = `
scalar Long
scalar BigDecimal

type Query {
    products: [Product]
    expiredProducts: [Product]
    availableProducts: [Product]
}

type Mutation {
    createProduct(product: CreateProduct): Product
    updateProduct(id: Long!, product: UpdateProduct!): Product
    deleteProduct(id: Int!): Product
}

type Product {
    id: Long
    purchase_price: BigDecimal
    sale_price: BigDecimal
    initial_amount: Int
    current_amount: Int
    product_definition: ProductDefinition
    created_at: DateTime
    modified_at: DateTime
}
input CreateProduct {
    purchase_price: Float!
    sale_price: Float!
    initial_amount: Int!
    current_amount: Int!
    product_definition_id: Int!
}
input UpdateProduct {
    purchase_price: BigDecimal
    sale_price: BigDecimal
    initial_amount: Int
    current_amount: Int
    product_definition_id: Int
}
`;
