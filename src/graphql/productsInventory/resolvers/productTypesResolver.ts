import * as productTypesService from "../../../services/productsInventory/productTypesService.ts";
import { findOneById as findOneIvaPercentageById } from "../../../services/productsInventory/ivaPercentagesService.ts";
import { DB_INACTIVE } from "../../../constants/exceptions.ts";
import { KnownGQLError } from "../../../utilities/KnownGQLError.ts";
import {
  CONNECTION_REFUSED,
  DUPLICATE,
} from "../../../constants/databaseKeywords.ts";
import { PRODUCT_TYPE_ALREADY_CREATED } from "../../../constants/productsInventory/productTypes/exceptions.ts";
import { NON_REGISTERED } from "../../../constants/exceptions.ts";
import {
  PRODUCT_TYPES,
} from "../../../constants/productsInventory/productTypes/keywords.ts";
import { IVA_PERCENTAGE } from "../../../constants/productsInventory/ivaPercentages/keywords.ts";

export const productTypesResolver = {
  Query: {
    productTypes: async () => {
      try {
        return await productTypesService.findAll();
      } catch (error) {
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        throw error;
      }
    },
  },
  Mutation: {
    createProductType: async (
      _: unknown,
      {
        productType,
      }: {
        productType: {
          name: string;
          is_expirable: boolean;
          iva_percentage_id: number;
        };
      },
    ) => {
      try {
        return await productTypesService.createOne(
          productType.name,
          productType.is_expirable,
          productType.iva_percentage_id,
        );
      } catch (error) {
        if (
          error.message.toLowerCase().includes(DUPLICATE)
        ) {
          throw new KnownGQLError({
            nontechnical: PRODUCT_TYPE_ALREADY_CREATED,
            technical: error.message,
          });
        }
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        throw error;
      }
    },
    updateProductType: async (
      _: unknown,
      {
        id,
        productType,
      }: {
        id: number;
        productType: {
          name: string;
          is_expirable: boolean;
          iva_percentage_id: number;
        };
      },
    ) => {
      try {
        return await productTypesService.updateOneById(id)(
          productType.name,
          productType.is_expirable,
          productType.iva_percentage_id,
        );
      } catch (error) {
        if (
          error.message.toLowerCase().includes(DUPLICATE)
        ) {
          throw new KnownGQLError({
            nontechnical: PRODUCT_TYPE_ALREADY_CREATED,
            technical: error.message,
          });
        }
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        if (error.message.toLowerCase().includes(NON_REGISTERED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: PRODUCT_TYPES + ":" + id + NON_REGISTERED,
          });
        }
        throw error;
      }
    },
    deleteProductType: async (
      _: unknown,
      { id }: { id: number },
    ) => {
      try {
        return await productTypesService.deleteOneById(id);
      } catch (error) {
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        if (error.message.toLowerCase().includes(NON_REGISTERED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: PRODUCT_TYPES + ":" + id + NON_REGISTERED,
          });
        }
        throw error;
      }
    },
  },
  ProductType: {
    iva_percentage: async (productType: { iva_percentage_id: number }) => {
      try {
        return await findOneIvaPercentageById(productType.iva_percentage_id);
      } catch (error) {
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        if (error.message.toLowerCase().includes(NON_REGISTERED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: IVA_PERCENTAGE + ":" + productType.iva_percentage_id +
              NON_REGISTERED,
          });
        }
        throw error;
      }
    },
  },
};
