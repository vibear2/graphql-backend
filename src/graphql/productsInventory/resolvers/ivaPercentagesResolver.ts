import * as ivaPercentagesService from "../../../services/productsInventory/ivaPercentagesService.ts";
import { KnownGQLError } from "../../../utilities/KnownGQLError.ts";
import { DB_INACTIVE } from "../../../constants/exceptions.ts";
import { CONNECTION_REFUSED } from "../../../constants/databaseKeywords.ts";

export const ivaPercentagesResolver = {
  Query: {
    ivaPercentages: async () => {
      try {
        return await ivaPercentagesService.findAll();
      } catch (error) {
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        throw error;
      }
    },
  },
};
