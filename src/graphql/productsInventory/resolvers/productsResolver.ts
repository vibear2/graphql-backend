import * as productsService from "../../../services/productsInventory/productsService.ts";
import { findOneById as findOneProductDefinitionById } from "../../../services/productsInventory/productDefinitionsService.ts";
import { KnownGQLError } from "../../../utilities/KnownGQLError.ts";
import {
  CONNECTION_REFUSED,
  DUPLICATE,
} from "../../../constants/databaseKeywords.ts";
import { DB_INACTIVE, NON_REGISTERED } from "../../../constants/exceptions.ts";
import { PRODUCT_ALREADY_CREATED } from "../../../constants/productsInventory/products/exceptions.ts";
import { PRODUCT } from "../../../constants/productsInventory/products/keywords.ts";
import { PRODUCT_DEFINITIONS } from "../../../constants/productsInventory/productDefinitions/keywords.ts";

export const productsResolver = {
  Query: {
    products: async () => {
      try {
        return await productsService.findAll();
      } catch (error) {
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        throw error;
      }
    },
    expiredProducts: async () => {
      try {
        return await productsService.findExpired();
      } catch (error) {
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        throw error;
      }
    },
    availableProducts: async () => {
      try {
        return await productsService.findAvailable();
      } catch (error) {
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        throw error;
      }
    },
  },
  Mutation: {
    createProduct: async (
      _: unknown,
      {
        product,
      }: {
        product: {
          purchase_price: number;
          sale_price: number;
          initial_amount: number;
          current_amount: number;
          product_definition_id: number;
        };
      },
    ) => {
      try {
        return await productsService.createOne(
          product.purchase_price,
          product.sale_price,
          product.initial_amount,
          product.current_amount,
          product.product_definition_id,
        );
      } catch (error) {
        if (
          error.message.toLowerCase().includes(DUPLICATE)
        ) {
          throw new KnownGQLError({
            nontechnical: PRODUCT_ALREADY_CREATED,
            technical: error.message,
          });
        }
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        throw error;
      }
    },
    updateProduct: async (
      _: unknown,
      {
        id,
        product,
      }: {
        id: number;
        product: {
          purchase_price: number;
          sale_price: number;
          initial_amount: number;
          current_amount: number;
          product_definition_id: number;
        };
      },
    ) => {
      try {
        return await productsService.updateOneById(id)(
          product.purchase_price,
          product.sale_price,
          product.initial_amount,
          product.current_amount,
          product.product_definition_id,
        );
      } catch (error) {
        if (
          error.message.toLowerCase().includes(DUPLICATE)
        ) {
          throw new KnownGQLError({
            nontechnical: PRODUCT_ALREADY_CREATED,
            technical: error.message,
          });
        }
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        if (error.message.toLowerCase().includes(NON_REGISTERED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: PRODUCT + ":" + id + NON_REGISTERED,
          });
        }
        throw error;
      }
    },
    deleteProduct: async (
      _: unknown,
      { id }: { id: number },
    ) => {
      try {
        return await productsService.deleteOneById(id);
      } catch (error) {
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        if (error.message.toLowerCase().includes(NON_REGISTERED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: PRODUCT + ":" + id + NON_REGISTERED,
          });
        }
        throw error;
      }
    },
  },
  Product: {
    product_definition: async (product: { product_definition_id: number }) => {
      try {
        return await findOneProductDefinitionById(
          product.product_definition_id,
        );
      } catch (error) {
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        if (error.message.toLowerCase().includes(NON_REGISTERED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: PRODUCT_DEFINITIONS + ":" +
              product.product_definition_id + NON_REGISTERED,
          });
        }
        throw error;
      }
    },
  },
};
