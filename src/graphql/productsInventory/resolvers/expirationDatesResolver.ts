import * as expirationDatesService from "../../../services/productsInventory/expirationDatesService.ts";
import { findOneById as findOneProductById } from "../../../services/productsInventory/productsService.ts";
import { KnownGQLError } from "../../../utilities/KnownGQLError.ts";
import {
  CONNECTION_REFUSED,
  DUPLICATE,
} from "../../../constants/databaseKeywords.ts";
import { DB_INACTIVE, NON_REGISTERED } from "../../../constants/exceptions.ts";
import { EXPIRATION_DATE_ALREADY_CREATED } from "../../../constants/productsInventory/expirationDates/exceptions.ts";
import { EXPIRATION_DATE } from "../../../constants/productsInventory/expirationDates/keywords.ts";
import { PRODUCT } from "../../../constants/productsInventory/products/keywords.ts";

export const expirationDatesResolver = {
  Query: {
    expirationDates: async () => {
      try {
        return await expirationDatesService.findAll();
      } catch (error) {
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        throw error;
      }
    },
  },
  Mutation: {
    createExpirationDate: async (
      _: unknown,
      {
        expirationDate,
      }: {
        expirationDate: { value: Date; product_id: number };
      },
    ) => {
      try {
        return await expirationDatesService.createOne(
          expirationDate.value,
          expirationDate.product_id,
        );
      } catch (error) {
        if (
          error.message.toLowerCase().includes(DUPLICATE)
        ) {
          throw new KnownGQLError({
            nontechnical: EXPIRATION_DATE_ALREADY_CREATED,
            technical: error.message,
          });
        }
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        throw error;
      }
    },
    updateExpirationDate: async (_: unknown, {
      id,
      expirationDate,
    }: {
      id: number;
      expirationDate: { value: Date; product_id: number };
    }) => {
      try {
        return await expirationDatesService.updateOneById(id)(
          expirationDate.value,
          expirationDate.product_id,
        );
      } catch (error) {
        if (
          error.message.toLowerCase().includes(DUPLICATE)
        ) {
          throw new KnownGQLError({
            nontechnical: EXPIRATION_DATE_ALREADY_CREATED,
            technical: error.message,
          });
        }
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        if (error.message.toLowerCase().includes(NON_REGISTERED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: EXPIRATION_DATE + ":" + id +
              NON_REGISTERED,
          });
        }
        throw error;
      }
    },
    deleteExpirationDate: async (
      _: unknown,
      { id }: { id: number },
    ) => {
      try {
        return await expirationDatesService.deleteOneById(id);
      } catch (error) {
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        if (error.message.toLowerCase().includes(NON_REGISTERED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: EXPIRATION_DATE + ":" + id +
              NON_REGISTERED,
          });
        }
        throw error;
      }
    },
  },
  ExpirationDate: {
    product: async (expirationDate: { product_id: number }) => {
      try {
        return await findOneProductById(expirationDate.product_id);
      } catch (error) {
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        if (error.message.toLowerCase().includes(NON_REGISTERED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: PRODUCT + ":" + expirationDate.product_id +
              NON_REGISTERED,
          });
        }
        throw error;
      }
    },
  },
};
