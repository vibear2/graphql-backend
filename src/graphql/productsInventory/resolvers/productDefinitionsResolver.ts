import * as productDefinitionService from "../../../services/productsInventory/productDefinitionsService.ts";
import { findOneById as findOneProductTypeById } from "../../../services/productsInventory/productTypesService.ts";
import { KnownGQLError } from "../../../utilities/KnownGQLError.ts";
import {
  CONNECTION_REFUSED,
  DUPLICATE,
} from "../../../constants/databaseKeywords.ts";
import { DB_INACTIVE, NON_REGISTERED } from "../../../constants/exceptions.ts";
import { PRODUCT_DEFINITION_ALREADY_CREATED } from "../../../constants/productsInventory/productDefinitions/exceptions.ts";
import { PRODUCT_DEFINITIONS } from "../../../constants/productsInventory/productDefinitions/keywords.ts";
import { PRODUCT_TYPES } from "../../../constants/productsInventory/productTypes/keywords.ts";

export const productDefinitionsResolver = {
  Query: {
    productDefinitions: async () => {
      try {
        return await productDefinitionService.findAll();
      } catch (error) {
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        throw error;
      }
    },
  },
  Mutation: {
    createProductDefinition: async (
      _: unknown,
      {
        productDefinition,
      }: {
        productDefinition: {
          name: string;
          description: string;
          product_type_id: number;
        };
      },
    ) => {
      try {
        return await productDefinitionService.createOne(
          productDefinition.name,
          productDefinition.description,
          productDefinition.product_type_id,
        );
      } catch (error) {
        if (
          error.message.toLowerCase().includes(DUPLICATE)
        ) {
          throw new KnownGQLError({
            nontechnical: PRODUCT_DEFINITION_ALREADY_CREATED,
            technical: error.message,
          });
        }
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        throw error;
      }
    },
    updateProductDefinition: async (
      _: unknown,
      {
        id,
        productDefinition,
      }: {
        id: number;
        productDefinition: {
          name: string;
          description: string;
          product_type_id: number;
        };
      },
    ) => {
      try {
        return await productDefinitionService.updateOneById(
          id,
        )(
          productDefinition.name,
          productDefinition.description,
          productDefinition.product_type_id,
        );
      } catch (error) {
        if (
          error.message.toLowerCase().includes(DUPLICATE)
        ) {
          throw new KnownGQLError({
            nontechnical: PRODUCT_DEFINITION_ALREADY_CREATED,
            technical: error.message,
          });
        }
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        if (error.message.toLowerCase().includes(NON_REGISTERED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: PRODUCT_DEFINITIONS + ":" + id +
              NON_REGISTERED,
          });
        }
        throw error;
      }
    },
    deleteProductDefinition: async (
      _: unknown,
      { id }: { id: number },
    ) => {
      try {
        return await productDefinitionService.deleteOneById(id);
      } catch (error) {
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        if (error.message.toLowerCase().includes(NON_REGISTERED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: PRODUCT_DEFINITIONS + ":" + id +
              NON_REGISTERED,
          });
        }
        throw error;
      }
    },
  },
  ProductDefinition: {
    product_type: async (productDefinition: { product_type_id: number }) => {
      try {
        return await findOneProductTypeById(productDefinition.product_type_id);
      } catch (error) {
        if (error.message.toLowerCase().includes(CONNECTION_REFUSED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: DB_INACTIVE,
          });
        }
        if (error.message.toLowerCase().includes(NON_REGISTERED)) {
          throw new KnownGQLError({
            technical: error.message,
            nontechnical: PRODUCT_TYPES + ":" +
              productDefinition.product_type_id +
              NON_REGISTERED,
          });
        }
        throw error;
      }
    },
  },
};
