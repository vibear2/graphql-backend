import { pool as db } from "../../db/connection.ts";
import { NON_REGISTERED } from "../../constants/exceptions.ts";
import { EXPIRATION_DATE } from "../../constants/productsInventory/expirationDates/keywords.ts";

export const findAll = async () => {
  const result = await db.query(
    "select * from products_inventory.expiration_dates order by modified_at desc",
  );
  return result.rows;
};

export const createOne = async (
  value: Date,
  product_id: number,
) => {
  const result = await db.query(
    `insert into products_inventory.expiration_dates (value, product_id) values ('${value}', ${product_id}) returning *;`,
  );
  return result.rows[0];
};

const findOneById = async (id: number) => {
  const result = await db.query(
    `select ed.* from products_inventory.expiration_dates ed where ed.id = ${id};`,
  );
  if (result.rowCount === 0) {
    throw new Error(EXPIRATION_DATE + ":" + id + NON_REGISTERED);
  }
  return result.rows[0];
};

export const updateOneById = (id: number) => {
  return async (
    value: Date,
    product_id: number,
  ) => {
    const previousValue = await findOneById(id);
    const result = await db.query(
      `update products_inventory.expiration_dates set value = '${
        value === undefined ? previousValue.rows[0].value : value
      }', product_id = ${
        product_id === undefined ? previousValue.rows[0].product_id : product_id
      }, updated_at = now()
      where id = ${id} returning *;`,
    );
    return result.rows[0];
  };
};

export const deleteOneById = async (id: number) => {
  const result = await db.query(
    `delete from products_inventory.expiration_dates where id = ${id} returning *;`,
  );
  if (result.rowCount === 0) {
    throw new Error(EXPIRATION_DATE + ":" + id + NON_REGISTERED);
  }
  return result.rows[0];
};
