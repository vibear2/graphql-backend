import { pool as db } from "../../db/connection.ts";
import { PRODUCT_DEFINITIONS } from "../../constants/productsInventory/productDefinitions/keywords.ts";
import { NON_REGISTERED } from "../../constants/exceptions.ts";

export const findAll = async () => {
  const response = await db.query(
    "select pd.* from products_inventory.product_definitions pd order by pd.modified_at desc;",
  );
  return response.rows;
};

export const createOne = async (
  name: string,
  description: string,
  product_type_id: number,
) => {
  const result = await db.query(
    `insert into products_inventory.product_definitions ("name", description, product_type_id) values ('${name}', '${description}', ${product_type_id}) returning *;
  `,
  );
  return result.rows[0];
};

export const findOneById = async (id: number) => {
  const result = await db.query(
    `select pd.* from products_inventory.product_definitions pd where pd.id = ${id};`,
  );
  if (result.rowCount === 0) {
    throw new Error(PRODUCT_DEFINITIONS + ":" + id + NON_REGISTERED);
  }
  return result.rows[0];
};

export const updateOneById = (id: number) => {
  return async (
    name: string,
    description: string,
    product_type_id: number,
  ) => {
    const previousValue = await findOneById(id);
    const result = await db.query(
      `update products_inventory.product_definitions set "name" = '${
        name === undefined ? previousValue.rows[0].name : name
      }', description = '${
        description === undefined
          ? previousValue.rows[0].description
          : description
      }', product_type_id = ${
        product_type_id === undefined
          ? previousValue.rows[0].product_type_id
          : product_type_id
      }, modified_at = now() 
      where id = ${id} returning *;`,
    );
    return result.rows[0];
  };
};

export const deleteOneById = async (id: number) => {
  const result = await db.query(
    `delete from products_inventory.product_definitions where id = ${id} returning *;`,
  );
  if (result.rowCount === 0) {
    throw new Error(PRODUCT_DEFINITIONS + ":" + id + NON_REGISTERED);
  }
  return result.rows[0];
};
