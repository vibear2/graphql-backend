import { pool as db } from "../../db/connection.ts";
import { NON_REGISTERED } from "../../constants/exceptions.ts";
import {
  PRODUCT_TYPES,
} from "../../constants/productsInventory/productTypes/keywords.ts";

export const findAll = async () => {
  const result = await db.query(
    "select pt.* from products_inventory.product_types pt order by pt.modified_at desc;",
  );
  return result.rows;
};

export const createOne = async (
  name: string,
  is_expirable: boolean,
  iva_percentage_id: number,
) => {
  const result = await db.query(
    `insert into products_inventory.product_types ("name", is_expirable, iva_percentage_id) values ('${name}', ${is_expirable}, ${iva_percentage_id}) returning *;
    `,
  );
  return result.rows[0];
};

export const findOneById = async (id: number) => {
  const result = await db.query(
    `select pt.* from products_inventory.product_types pt where pt.id = ${id};`,
  );
  if (result.rowCount === 0) {
    throw new Error(PRODUCT_TYPES + ":" + id + NON_REGISTERED);
  }
  return result.rows[0];
};

export const updateOneById = (id: number) => {
  return async (
    name: string,
    is_expirable: boolean,
    iva_percentage_id: number,
  ) => {
    const previousValue = await findOneById(id);
    const result = await db.query(
      `update products_inventory.product_types set "name" = '${
        name === undefined ? previousValue.name : name
      }', is_expirable = ${
        is_expirable === undefined ? previousValue.is_expirable : is_expirable
      }, iva_percentage_id = ${
        iva_percentage_id === undefined
          ? previousValue.iva_percentage_id
          : iva_percentage_id
      }, modified_at = now()
       where id = ${id} returning *;`,
    );
    return result.rows[0];
  };
};

export const deleteOneById = async (id: number) => {
  const result = await db.query(
    `delete from products_inventory.product_types where id = ${id} returning *;`,
  );
  if (result.rowCount === 0) {
    throw new Error(PRODUCT_TYPES + ":" + id + NON_REGISTERED);
  }
  return result.rows[0];
};
