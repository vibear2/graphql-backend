import { pool as db } from "../../db/connection.ts";
import { NON_REGISTERED } from "../../constants/exceptions.ts";
import { IVA_PERCENTAGE } from "../../constants/productsInventory/ivaPercentages/keywords.ts";

export const findAll = async () => {
  const result = await db.query(
    "select ip.* from products_inventory.iva_percentages ip order by ip.modified_at desc;",
  );
  return result.rows;
};

export const findOneById = async (id: number) => {
  const result = await db.query(
    `select ip.* from products_inventory.iva_percentages ip where ip.id = ${id};`,
  );
  if (result.rowCount === 0) {
    throw new Error(IVA_PERCENTAGE + ":" + id + NON_REGISTERED);
  }
  return result.rows[0];
};
