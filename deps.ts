//dmm deno modules manager could help with modules management

// Oak
export { Application, Router } from "https://deno.land/x/oak@v9.0.0/mod.ts";
export type { RouterContext } from "https://deno.land/x/oak@v9.0.0/mod.ts";

//
export { Pool } from "https://deno.land/x/pg@v0.6.0/mod.ts";

//
export {
  applyGraphQL,
  GQLError,
} from "https://deno.land/x/oak_graphql@0.6.2/mod.ts";

//
export { oakCors } from "https://deno.land/x/cors@v1.2.2/mod.ts";

//
export { config } from "https://deno.land/x/dotenv@v3.0.0/mod.ts";

//
export {
  mergeResolvers,
  mergeTypeDefs,
} from "https://cdn.skypack.dev/@graphql-tools/merge";
