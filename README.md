# viBear's Products Inventory GraphQL API.
## Description

This is the API that provides the backend for the products inventory module of the viBear Retail Management System, is built with GrpahQL and Typescript. It's runtime is deno.

## Showcase

1. ivaPercentages
   1. Query ![query of iva percentages](public/assets/images/showcase_1.png)
2. productTypes
   1. Query ![query of product types](public/assets/images/productTypes_query.png)
   2. Mutation
      1. Create ![mutation of product types, create](public/assets/images/productTypes_mutation_create.png)
      2. Update ![mutation of product types, update](public/assets/images/productTypes_mutation_update.png)
      3. Delete ![mutation of product types, delete](public/assets/images/productTypes_mutation_delete.png)
3. (...) Others were not included to avoid redundancy.

## Database schema

![database schema](public/assets/images/products_inventory_schema.png)

### iva_percentages

This table is meant to be an administrative table, contains the percentage of the value of the product that is going to be paid as a fee "Impuesto al Valor Agregado".

#### Example

| id  | value |
| --- | ----- |
| 1   | 0     |
| 2   | 12    |

### product_types

This table is meant to contain the many types of products that are going to be used in the system. A relationship of one to many is placed whereas one "iva percentage" belongs to many "product types".
| id | name | is_expirable | iva_percentage_id |
| - | - | - | - |
| 1 | lácteos | true | 1 |
| 2 | cárnicos | true | 2 |
| 3 | utensilios de limpieza | false | 2 |

### product_definitions

This table is meant to contain data of the classes of products that can be filled in the inventory and managed by it. A relationship of one to many is placed whereas one "product type" belongs to many "product definitions"
| id | name | description | iva_percentage_id |
| - | - | - | - |
| 1 | Leche Floralp 800ml | Funda de leche enter de tamaño mediano. | 1 |
| 2 | Mortadela sanduchera Juris 60g | Paquete de mortadelas, con contenido de seis unidades | 2 |
| 3 | Cepillo dental Colgate pequeño | Colores disponibles: amarillo, rojo y azul | 2|

### products

This table is meant to contain the amount of and other details of a set of products, is where the inventory is managed. It is built that ways so the repetition of data is avoided by only filling new set of products that might difer of another by its price of purchase and sell. One "product definition" belongs to many "products"
|id|purchase_price|sale_price|initial_amount|current_amount|product_definition_id|is_active|
|-|-|-|-|-|-|-|
|1|0.90|1.00|20|16|1|false|
|2|0.85|1.00|10|5|1|true|
|3|0.55|0.65|15|10|2|true|
|4|0.75|0.85|24|14|3|true|

### expiration_dates

This table is meant to contain the id of the products and its expiration dates in the case of the product being expirable, it is present to avoid nullity of data in the case of the field expiration_date being in the products table itself, because not all the products would have expiration dates and either the field should be nullable or contain non valuable data such as a non real date. One "product" belongs to one "expiration date".
|id|product_id|value|
|-|-|-|
|1|1|14-12-2019|
|2|2|14-12-2030|
|3|3|14-12-2030|

## Runtime flags

- `--allow-env`
- `--allow-net`
- `--allow-read`

## Available environment variables with examples.

This variables are meant to be placed in a .env file in the root of the project.

- POSTGRESQL_USER=postgres
- POSTGRESQL_HOST=localhost
- POSTGRESQL_DB_NAME=vibear
- POSTGRESQL_PASSWORD=admin
- POSTGRESQL_PORT=5432
- PORT=4000
- USE_PLAYGROUND=true

## How to run

1. Install deno  
   The installation method depends on the development or production environment that you are running. Further instructions are provided [here](https://deno.land/#installation).
2. Install and setup PostgresQL  
   The installation method depends on the development or production environment that you are running. Furter instructions are provided [here](https://www.postgresql.org/download/).
3. Clone the repository  
   `git clone https://gitlab.com/vibear2/graphql-backend.git`
4. Regenerate the database schema.  
   The database's schema has been built using [dbdiagram](https://dbdiagram.io/) and [dbml](https://www.dbml.org/home/) in order to provide the ability to use other relational databases.
   1. Copy the content of the file dbml/products_inventory.dbml to your clipboard. ![products inventory dbml](public/assets/images/products_inventory_dbml.png)
   2. [Enter](https://dbdiagram.io/d) dbdiagram's app.
   3. Paste the content that you just copied to the text area in the left side of [dbdiagram](https://dbdiagram.io/d).
      ![products inventory dbml schema on dbdiagram](public/assets/images/products_inventory_on_dbdiagram.png)
   4. Export the diagram to a postgresql dml schema (it is required to create an account).
      ![products inventory postgresql dmk schema](public/assets/images/products_inventory_sql_dml.png)
   5. Create a new database.
      ```sql
      create database vibear;
      ```
   6. Connect to or select the just created database.
   7. Create a proper schema, the given name must be used.
      ```sql
      create schema products_inventory;
      ```
   8. Select the just created schema.
   9. Run the script that you exported from [dbdiagram](https://dbdiagram.io/d).
   10. Run the [constraints](dbml/products_inventory.constraints).
5. Create a file named `.env` in the root of the project.
6. Write in the file the proper environment variables, examples are provided above.
7. Run the project with the command: `deno run --allow-env --allow-net --allow-read src/index.ts` ![successful run](public/assets/images/successful_run.png)

## How to start developing

Having ran the project for the first time, you must install [denon](https://deno.land/x/denon), do not forget to set up your path properly. You can start the development server with: `denon start`.

## References

- [Previous repository](https://github.com/MiguelRodrii/viBear-back)
